<?php

session_start();
require_once "inc/config.inc.php";
require_once "inc/functions.inc.php";
require_once "inc/SEPAprocedure.inc.php";

//Überprüfe, dass der User eingeloggt ist
//Der Aufruf von check_user() muss in alle internen Seiten eingebaut sein
$user = check_user();
check_consul();

require 'vendor/autoload.php';

require_once "inc/Template.php";
require_once "inc/Bill.inc.php";
use Dompdf\Dompdf;

require_once "controller/InvoiceController.php";
$controller = new InvoiceController($pdo);

if(array_key_exists('mode', $_POST) && $_POST['mode'] === "create_invoices") {

    $invoicableUsers = $controller->getInvoicableUsers(20);

    foreach ($invoicableUsers as $u) {
        // When a user is deleted, the required data for making a proper invoice isn't available anymore
        if ($u['first_name'] == "deleted") {
            continue;
        }
        $uid = $u['uid'];

        $pdo->beginTransaction();

        try {
            $order_items = $controller->getInvoicableOrdersByUser($uid);
        } catch(Exception $e) {
            error(json_encode($e->getMessage()));
            $pdo->rollBack();
            continue;
        }

        // We don't need to create an invoice if there are only empty orders
        if(count($order_items) > 0) {

            $invoice_id = $controller->createInvoice($uid);

            try {
                $bill = new Bill(
                    $invoice_id,
                    date("Y-m-d h:i"),
                    // TODO make user object
                    $u['organization'],
                    $u['first_name'],
                    $u['last_name'],
                    $u['street'],
                    $u['street_number'],
                    $u['postal_code'],
                    $u['region'],
                    $u['email'],
                    $order_items
                );

                ##################### Create PDFs #####################
                $dompdf = new Dompdf();

                $dompdf->loadHtml($bill->bill);
                $dompdf->setPaper('A4', 'portrait');
                $dompdf->render();
                $invoice_output = $dompdf->output();
                file_put_contents('invoices/Rechnung_' . $invoice_id . '.pdf', $invoice_output);
            } catch (Exception $e) {
                $pdo->rollBack();
                continue;
            }

            foreach ($order_items as $o) {
                try {
                    $controller->markAsInvoiced($o['oid'], $invoice_id);
                } catch (Exception $e) {
                    $pdo->rollBack();
                    continue;
                }
            }
        } else {
            $pdo->rollBack();
            continue;
        }
        $pdo->commit();
        // TODO notify success
    }
}
if(array_key_exists('mode', $_GET) && $_GET['mode'] === "download") {
    $file = 'invoices/Rechnung_' . $_GET['id'] . '.pdf';

    // Check if the file exists
    if (file_exists($file)) {
        // Set headers to force download
        header('Content-Description: File Transfer');
        header('Content-Type: application/pdf');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        // Clear output buffer
        ob_clean();
        flush();

        // Read the file and send it to the output buffer
        readfile($file);
        exit;
    } else {
        echo "File not found.";
    }
}
if(array_key_exists('mode', $_POST) && $_POST['mode'] === "create_sepa") {
    $creator = $user['uid'];
    $mails = [];
    $transactions = [];
    $uid = -1;
    // This sets the execution date of the sepa transfer. To be sure this will be in the future, we add a security interval of 4 days. This leaves plenty of room for processing.
    $today = date("Y-m-d");
    $collectionDt = date("Y-m-d", strtotime($today . ' + 4 days'));

    try {
        $pdo->beginTransaction();
        $sepa = new SEPAprocedure($pdo, $creator, $collectionDt, $myEntity, $myIBAN, $myBIC, $creditorId, $user['first_name'] . ' ' . $user['last_name']);

        $query = "SELECT invoices.id, users.uid, users.first_name, users.last_name, users.email, users.account_holder, users.IBAN, users.BIC, mandates.mid, mandates.created_at, SUM(order_items.quantity*CEILING((1+order_items.tax)*order_items.netto_price*100)/100) AS total FROM invoices
    LEFT JOIN orders on invoices.id = orders.invoice
    LEFT JOIN order_items on order_items.oid = orders.oid
    LEFT JOIN users on invoices.uid = users.uid
    LEFT JOIN mandates on invoices.uid = mandates.uid
    WHERE invoices.paid=0 AND invoices.id != 1
    GROUP BY id";
        $statement = $pdo->prepare($query);
        $result = $statement->execute();

        if (!$result) {
            throw new Exception('Fehler');
        }

        if ($statement->rowCount() > 0) {
            while ($row = $statement->fetch()) {

                // When a user is deleted, the required data for making a bank transfer isn't available anymore
                // We therefore skip adding the entry to the transfer list
                if ($row['IBAN'] == "deleted") {
                    continue;
                }

                if ($row['uid'] != $uid) {
                    $uid = $row['uid'];
                    $transactions[$uid]['first_name'] = $row['first_name'];
                    $transactions[$uid]['last_name'] = $row['last_name'];
                    $transactions[$uid]['email'] = $row['email'];
                    $transactions[$uid]['account_holder'] = $row['account_holder'];
                    $transactions[$uid]['IBAN'] = $row['IBAN'];
                    $transactions[$uid]['BIC'] = $row['BIC'];
                    $transactions[$uid]['mid'] = $row['mid'];
                    $transactions[$uid]['signed'] = substr($row['created_at'], 0, 10);
                    $transactions[$uid]['instdAmt'] = $row['total'];
                    $transactions[$uid]['rmtInf'] = "Rechnung Nr. " . $row['id'];
                } else {
                    $transactions[$uid]['instdAmt'] += $row['total'];
                    $transactions[$uid]['rmtInf'] .= " + " . $row['id'];

                    if (strlen($transactions[$uid]['rmtInf']) > 140) {
                        $transactions[$uid]['rmtInf'] = substr($transactions[$uid]['rmtInf'], 0, 139);
                    }
                }
                $statement2 = $pdo->prepare("UPDATE invoices SET paid = 1 WHERE id=:id");
                $result2 = $statement2->execute(array('id' => $row['id']));

                if (!$result2) {
                    throw new Exception(json_encode($statement2->errorInfo()));
                }
            }

            $sepa->insertTx($transactions);
            $sepa->create();
            //$sepa->notify($smtp_host, $smtp_username, $smtp_password, $myEmail, $myEntity);
            $pdo->commit();
            $sepa->startDownload();
        } else {
            notify("Keine offenen Zahlungen gefunden.");
        }
    } catch (Exception $e) {
        $pdo->rollBack();
        error($e->getMessage());
    }
}

$invoices = $controller->getInvoices();

include "templates/header.inc.php";
include "templates/nav.inc.php";
include "templates/admin-nav.inc.php";

$openInvoices = $controller->getInvoicableUsers();
?>

<div class="container spacer">

  <span>Erzeugbare Rechnungen: <?php e(count($openInvoices)); ?></span>
  <form action="/invoices.php" method="POST" style="margin-bottom: 20px;">
    <button class="btn" name="mode" value="create_invoices"><i class="fa fa-plus"></i> Rechnungen erzeugen</button>
    <button class="btn" name="mode" value="create_sepa">Lastschrift erzeugen</button>
  </form>
  <table class="table">
    <tr>
      <th>ID</th>
      <th>Mitglied</th>
      <th>bezahlt</th>
      <th>Datum</th>
      <th>Download</th>
    </tr>
    <?php foreach ($invoices as $invoice) : ?>
    <tr>
      <td><?php e($invoice['id']) ?></td>
      <td><?php e($invoice['first_name'] . " " . $invoice['last_name']); ?></td>
      <td>
        <?php if($invoice['paid']) : ?>
        <i class="fa fa-check"></i>
        <?php else: ?>
        <i class="fa fa-times"></i>
        <?php endif; ?>
      </td>
      <td><?php datetime($invoice['date']) ?></td>
      <td><a href="/invoices.php?mode=download&id=<?php echo $invoice['id'] ?>">Download</a></td>
    </tr>
    <?php endforeach; ?>
  </table>
</div>

<?php
include "templates/footer.inc.php"
?>



