<?php
require_once(__DIR__."/../inc/Template.php");
?>
<!DOCTYPE html>
<html lang="de">
<h1>Moin, <?php e($invoice['first_name']) ?>!</h1>
  <p>Für deine letzten Einkäufe ziehen wir <?php currency($invoice['total']) ?> von deinem Konto mit der IBAN <?php e(mask($invoice['IBAN'], 8)) ?> bei der Bank <?php e($invoice['BIC']) ?> ein.</p>
<p>Die Abbuchung erfolgt als SEPA-Lastschrift unter dem Mandat mit der Referenznummer <?php e($invoice['mid']) ?> mit der Gläubiger-Identifikationsnummer <?php e($creditorId) ?>. Es werden alle Bestellungen bis zum <?php day($invoice['date']) ?> beglichen.</p>
<p>Die Rechnung befindet sich im Anhang.</p>
</html>
