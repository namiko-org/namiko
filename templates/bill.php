<?php
require_once(__DIR__."/../inc/Template.php");
/*
$first_name="test";
$last_name ="test";
$street = "test street";
$street_number = 123;
$postal_code = 12345;
$region = "Region";
$email = "ksjadk@skjdfjk.de";
$invoice_id = 1234;
$created_at = "2023-09-07 12:43:12";
*/
?>
<!DOCTYPE html>

<head>
  <style>
  body {
  font-family: sans-serif; 
  font-size: 0.8em;
  margin: 0;
  padding: 10mm 10mm 0;
  }
  p {
  margin: 0;
  }
  th, td {
  text-align: left;
  }
  .right {
  text-align: right;
  }
  #title {
  display: flex;
  justify-content: flex-start;
  align-items: baseline;
  margin-top: 12em;
  }
  h1, h2 {
  display: inline;
  margin-right: 1em;
  line-height: 30px;
  vertical-align: middle;
  }
  h1 {
  font-size: 1.5em;
  }
  h2 {
  font-size: 1em;
  }

  table {
  border-bottom: 1px solid black;
  }
  table#sum {
  border-bottom: thick double black;
  float: right;
  }
  </style>

</head>
<body>


  <div style="float: left">
    <p><?php e($first_name . " " . $last_name) ?></p>
    <p><?php e($street . " " . $street_number) ?></p>
    <p><?php e($postal_code . " " . $region) ?></p>
    <p><?php e($email) ?></p>
  </div>

  <div style="float: right;">
    <p><?php e($myEntity ?? "") ?></p>
    <p><?php e($myStreet ?? "") ?></p>
    <p><?php e($myRegion ?? "") ?></p>
    <p><?php e($myEmail ?? "") ?></p>
    <p><?php e($myWebsite ?? "") ?></p>
    <p><?php e($myIBAN ?? "") ?></p>
    <p><?php e($myBIC ?? "") ?></p>
    <p><?php e($tax_number ?? "") ?></p>
  </div>
  <div style="clear: both"></div>

  <div id="title">
    <h1>Rechnung #<?php e($invoice_id); ?></h1>
    <h2><?php datetime($created_at); ?></h2>
  </div>

  <table style="width: 100%; margin-top: 6em;">
    <tr style="font-weight: bold; margin-bottom: 10px">
      <th>Posten</th>
      <th>Artikel</th>
      <th class="right">Preis/E</th>
      <th class="right">Einheiten</th>
      <th class="right">Menge</th>
      <th class="right">Brutto Summe</th>
      <th class="right">Steuersatz</th>
    </tr>

    <?php foreach($items as $key => $i): ?>
    <tr>
      <td><?php e($key + 1) ?></td>
      <td><?php e($i['productName']) ?></td>
      <td class="right"><?php currency($i['brutto']) ?></td>
      <td class="right"><?php e(round($i['quantity'], 2)) ?></td>
      <td class="right"><?php e(round($i['unit_size'], 2) . ' ' . $i['unit_tag']) ?></td>
      <td class="right"><?php currency($i['brutto'] * $i['quantity']) ?></td>
      <td class="right"><?php percent($i['tax']) ?></td>
    </tr>
    <?php endforeach; ?>

  </table>

  <table id="sum">
    <?php foreach($taxes as $key => $tax): ?>
    <tr>
      <td>MwST. <?php percent($key) ?></td><td class="right"><?php currency($tax) ?></td>
    </tr>
    <?php endforeach; ?>
    <tr style="font-weight: bold">
      <td>Gesamt Brutto</td><td class="right"><?php currency($sum['brutto']) ?></td>
    </tr>
  </table>
</body>
