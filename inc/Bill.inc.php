<?php

class Bill
{
    public $bill = "";
    private $grandtotal = 0;
    private $count = 1;

    public function __construct($invoice_id, $created_at, $organization, $first_name, $last_name, $street, $street_number, $postal_code, $region, $email, $items)
    {
        global $myEntity;
        global $myStreet;
        global $myRegion;
        global $myEmail;
        global $myWebsite;
        global $myIBAN;
        global $myBIC;
        global $tax_number;

        $sum['brutto'] = 0;
        $taxes = array();
        foreach($items as $key => $item) {
            $items[$key]['brutto'] = $this->calculate_brutto($item['netto'], $item['tax']);
            $item_brutto_sum = $items[$key]['brutto'] * $item['quantity'];

            $item_netto_sum = $item['netto'] * $item['quantity'];

            $sum['brutto'] += $item_brutto_sum;
            if(!array_key_exists($item['tax'], $taxes)) {
                $taxes[$item['tax']] = 0;
            }
            $taxes[$item['tax']] += $item_netto_sum * $item['tax'];
        }

        ob_start();
        include(__DIR__."/../templates/bill.php");
        $this->bill = ob_get_contents();
        ob_end_clean();

    }

    public static function calculate_brutto(float $price, float $tax): float
    {
        $brutto = ceil($price * (1 + $tax) * 100) / 100;
        return $brutto;
    }
}
