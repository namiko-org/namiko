<?php

declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require __DIR__."/../inc/config.inc.php";
require __DIR__."/../inc/Template.php";

final class TemplateTest extends TestCase
{
    public function testEscapedOutput(): void
    {
        $this->expectOutputString('&lt; &gt; &amp; Test');
        e('< > & Test');
    }

    public function testCurrencyOutput(): void
    {
        $this->expectOutputString('10.21 €');
        currency(10.2001234);
    }

    public function testDatetimeOutput(): void
    {
        $this->expectOutputString('01.02.2003 04:05');
        datetime("2003-2-1 4:5:6");
    }

    public function testPercentOutput(): void
    {
        $this->expectOutputString('10%');
        percent(10);
    }

    public function testDecimalPercentOutput(): void
    {
        $this->expectOutputString('10%');
        percent(0.1);
    }

    public function testmaskString(): void
    {
        $this->assertSame(mask('abc1234', 3), 'abcXXXX');
    }
}
