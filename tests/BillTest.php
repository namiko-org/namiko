<?php

declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require __DIR__."/../inc/Bill.inc.php";

final class BillTest extends TestCase
{
    public function testBill(): void
    {
        $bill = new Bill(
            "1234",
            "2020-01-30 12:34:56",
            "Org",
            "First Name",
            "Second Name",
            "Straße",
            "9876",
            "12345",
            "Region",
            "example@email.address",
            [[
              "productName" => "Test Produkt",
              "unit_size" => 1,
              "unit_tag" => "kg",
              "netto" => 1,
              "quantity" => 1,
              "tax" => 0.09
            ]]
        );

        $this->assertStringContainsString("1234", $bill->bill);
    }
}
