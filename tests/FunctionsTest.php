<?php

declare(strict_types=1);
use PHPUnit\Framework\TestCase;

require __DIR__."/../inc/functions.inc.php";

final class FunctionsTest extends TestCase
{
    public function testGetSiteURL(): void
    {
        $_SERVER['HTTPS'] = 1;
        $_SERVER['HTTP_HOST'] = "test.host";
        $_SERVER['REQUEST_URI'] = "/path/to/file.php";

        $siteURL = getSiteURL();
        $this->assertStringStartsWith("https://", $siteURL);
        $this->assertStringContainsString("test.host", $siteURL);
        $this->assertStringEndsWith("/", $siteURL);
        $this->assertStringContainsString("/path/to", $siteURL);
        $this->assertSame(substr_count($siteURL, '//'), 1); // We don't want double slashes except in the protocol
        $this->assertSame(filter_var($siteURL, FILTER_VALIDATE_URL), $siteURL);
    }
}
