<?php

session_start();
require_once "inc/config.inc.php";
require_once "inc/functions.inc.php";

//Überprüfe, dass der User eingeloggt ist
//Der Aufruf von check_user() muss in alle internen Seiten eingebaut sein
$user = check_user();
check_consul();

require_once "controller/ProductController.php";
$controller = new ProductController($pdo);
$attachables = $controller->getAttachables();

require_once "controller/CategoryController.php";
$category_controller = new CategoryController($pdo);
$categories = $category_controller->getCategories();

require_once "controller/ProducerController.php";
$producer_controller = new ProducerController($pdo);
$producers = $producer_controller->getProducers();

$mode = $_REQUEST['mode'];
$error = "";

switch ($mode) {
    case 'edit':
        $id = intval($_REQUEST['id']);

        if ($_POST['submit'] ?? false) {
            $controller->updateProduct($id, [
              "productName" => $_POST["product_name"],
              "productDesc" => $_POST['product_description'],
              "netto" => $_POST['netto'],
              "price_KG_L" => $_POST['brutto'],
              "tax" => $_POST['tax'],
              "unit_size" => $_POST['unit_size'],
              "unit_tag" => $_POST['unit_tag'],
              "category" => $_POST['category'],
              "container" => $_POST['container'],
              "priceContainer" => $_POST['priceContainer'],
              "origin" => $_POST['origin'],
              "producer" => $_POST['producer'],
              "is_storage_item" => $_POST['is_storage_item'],
              "attached_product" => $_POST['attached_product']
            ]);
        }
        $product = $controller->getProduct($id);
        $product_brutto = $product->netto * (1 + $product->tax);
        break;
    default:
        e("Error, invalid mode");
        break;
}

include "templates/header.inc.php";
include "templates/nav.inc.php";
include "templates/admin-nav.inc.php";

require_once "inc/Template.php";

?>
<script type="text/javascript">
  function updateBrutto() {
    var netto = document.querySelector("[name=netto]").valueAsNumber;
    var tax = document.querySelector("[name=tax]").valueAsNumber;
    document.querySelector("input[name=brutto]").value = netto * (1 + tax);
  }
</script>
<main class="spacer">
  <a href="/admin.php"><i class="fa fa-chevron-left" aria-hidden="true"></i> Zurück</a>
  <?php if ($mode === "delete"): ?>
  <div class="message white bg-danger">
    Soll diese Kategorie wirklich gelöscht werden? Dieser Vorgang ist nicht umkehrbar!
  </div>
  <?php endif; ?>
  <?php if ($error !== ""): ?>
  <div class="message bg-warning">
    <?php e($error); ?>
  </div>
  <?php endif; ?>
  <form method="post">
    <div class="form-group">
      <label for="id">ID</label>
      <input type="number" name="id" id="id" class="form-control" value="<?php e($id); ?>" readonly />
    </div>
    <div class="form-group">
      <label for="product_name">Name</label>
      <input 
        type="text"
        name="product_name"
        id="product_name"
        class="form-control"
        value="<?php e($product->productName); ?>"
        <?php if ($mode === "delete"): ?>readonly<?php endif; ?>
      />
    </div>
    <div class="form-group">
      <label for="product_description">Beschreibung</label>
      <input 
        type="textarea"
        name="product_description"
        id="product_description"
        class="form-control"
        value="<?php e($product->productDesc); ?>"
        <?php if ($mode === "delete"): ?>readonly<?php endif; ?>
      />
    </div>
    <div class="form-group">
      <label for="category">Kategorie</label>
      <select name="category" id="category" class="form-control">
        <?php foreach($categories as $category): ?>
        <option 
          value="<?php e($category['cid']) ?>"
          <?php if($category['cid'] === $product->category): ?> selected <?php endif; ?>

        >
          <?php e($category['category_name']) ?>
        </option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="form-group">
      <label for="netto">Nettopreis</label>
      <input 
        type="number"
        name="netto"
        id="netto"
        step="0.01"
        min="0"
        class="form-control"
        value="<?php e($product->netto); ?>"
        <?php if ($mode === "delete"): ?>readonly<?php endif; ?>
        onchange="updateBrutto()"
      />
    </div>
    <div class="form-group">
      <label for="tax">Steuersatz</label>
      <input 
        type="number"
        name="tax"
        id="tax"
        step="0.01"
        min="0"
        class="form-control"
        value="<?php e($product->tax); ?>"
        <?php if ($mode === "delete"): ?>readonly<?php endif; ?>
        onchange="updateBrutto()"
      />
    </div>
    <div class="form-group">
      <label for="brutto">Bruttopreis</label>
      <input 
        type="number"
        name="brutto"
        id="brutto"
        class="form-control"
        value="<?php e($product_brutto); ?>"
        readonly
      />
    </div>
    <div class="form-group">
      <label for="unit_size">Einheitengröße</label>
      <input 
        type="number"
        name="unit_size"
        id="unit_size"
        step="1"
        min="0"
        class="form-control"
        value="<?php e($product->unit_size); ?>"
        <?php if ($mode === "delete"): ?>readonly<?php endif; ?>
      />
    </div>
    <div class="form-group">
      <label for="unit_tag">Einheitenkürzel</label>
      <input 
        type="text"
        name="unit_tag"
        id="unit_tag"
        class="form-control"
        value="<?php e($product->unit_tag); ?>"
        <?php if ($mode === "delete"): ?>readonly<?php endif; ?>
      />
    </div>
    <div class="form-group">
      <label for="container">Gebindegröße</label>
      <input 
        type="number"
        name="container"
        id="container"
        step="1"
        min="0"
        class="form-control"
        value="<?php e($product->container); ?>"
        <?php if ($mode === "delete"): ?>readonly<?php endif; ?>
      />
    </div>
    <div class="form-group">
      <label for="priceContainer">Gebindepreis</label>
      <input 
        type="number"
        name="priceContainer"
        id="priceContainer"
        step="0.01"
        min="0"
        class="form-control"
        value="<?php e($product->priceContainer); ?>"
        <?php if ($mode === "delete"): ?>readonly<?php endif; ?>
      />
    </div>
    <div class="form-group">
      <label for="origin">Herkunft</label>
      <input 
        type="text"
        name="origin"
        id="origin"
        class="form-control"
        value="<?php e($product->origin); ?>"
        <?php if ($mode === "delete"): ?>readonly<?php endif; ?>
      />
    </div>
    <div class="form-group">
      <label for="producer">Lieferant</label>
      <select name="producer" id="producer" class="form-control">
        <?php foreach($producers as $producer): ?>
        <option 
          value="<?php e($producer['pro_id']) ?>"
          <?php if($producer['pro_id'] === $product->producer): ?> selected <?php endif; ?>

        >
          <?php e($producer['producerName']) ?>
        </option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="form-group">
      <label for="is_storage_item">Lagerware</label>
      <input
        type="checkbox"
        name="is_storage_item"
        id="is_storage_item"
        class="form-control"
        value="1" 
        <?php if ($product->is_storage_item): ?>checked <?php endif; ?> 
        <?php if ($mode === "delete"): ?>disabled<?php endif; ?>
      />
    </div>
    <div class="form-group">
      <label for="attached_product">verknüpftes Produkt (z.B. Pfand)</label>
      <select name="attached_product" id="attached_product" class="form-control">
        <option value="">--</option>
        <?php foreach($attachables as $attachable): ?>
        <option
          value="<?php e($attachable['pid']) ?>"
          <?php if($attachable['pid'] === $product->attached_product): ?> selected <?php endif; ?>
        >
          <?php e($attachable['productName']) ?>
        </option>
        <?php endforeach; ?>
      </select>
    </div>
    <?php if ($mode === "delete"): ?>
    <button type="submit" name="submit" class="btn btn-danger" value="1">Löschen</button>
    <?php else: ?>
    <button type="submit" name="submit" class="btn btn-primary" value="1">Speichern</button>
    <?php endif; ?>
  </form>
  <h2 style="margin-top: 50px;">Statistiken</h2>
  <table class="table">
    <tr>
      <th>Jahr</th>
      <th>Monat</th>
      <th>Menge</th>
    </tr>
    <?php foreach($controller->getStatistics($id) as $stat): ?> 
      <tr>
        <td><?php e($stat['year']) ?></td>
        <td><?php e($stat['month']) ?></td>
        <td><?php e($stat['sum'] . " " . $stat['unit_tag']) ?></td>
      </tr>
    <?php endforeach; ?>
  </table>
</main>
